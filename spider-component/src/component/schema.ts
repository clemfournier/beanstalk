export interface WorkspaceOptions {
    path: string;
    project: string;
    name: string;
    inlineStyle: any;
    inlineTemplate: any;
    viewEncapsulation: any;
    changeDetection: any;
    prefix: string;
    styleext: any;
    style: any;
    spec: boolean;
    skipTests: boolean;
    flat: boolean;
    skipImport: boolean;
    selector: string;
    module: string;
    export: boolean;
    entryComponent: boolean;
    lintFix: boolean;
    minimal: any;
    projectRoot: any;
    experimentalIvy: boolean;
    skipPackageJson: boolean;
    routing: boolean;
    sourceDir : string;
}
