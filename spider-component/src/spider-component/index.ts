import {Rule, SchematicContext, Tree, chain, schematic} from '@angular-devkit/schematics';

export function spiderComponent(options: any): Rule {
    return chain([
        schematic('component', options),
        (tree: Tree, _context: SchematicContext) => {
            tree.getDir(options.sourceDir)
                .visit(filePath => {
                    if (!filePath.endsWith('.ts')) {
                        return;
                    }
                    const content = tree.read(filePath);
                    if (!content) {
                        return;
                    }

                    tree.overwrite(filePath, content);

                });
            return tree;
        },
    ]);
}
