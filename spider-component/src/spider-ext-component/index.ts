import {Rule, Tree, chain, externalSchematic} from '@angular-devkit/schematics';
import {WorkspaceOptions as ComponentOptions} from "../component/schema";

export function spiderExtComponent(options: ComponentOptions): Rule {
    return chain([
        externalSchematic('@schematics/angular', 'component', options),
        (tree: Tree) => {
            tree.getDir(options.sourceDir + '/app/' + options.name)
                .visit(filePath => {
                    const content = tree.read(filePath);

                    if (filePath.endsWith('component.ts') && content) {

                        let file = content.toString();
                        file = updateFile(file, "Component,", " OnDestroy,");
                        file = updateFile(file, "'@angular/core';", "\r\nimport { Subject } from 'rxjs';");
                        file = updateFile(file, "implements OnInit", ", OnDestroy");
                        file = updateFile(file, "OnDestroy {", "\r\n\r\n\  private _unsubscribeAll: Subject<any> = new Subject<any>();");
                        file = updateFile(file, "ngOnInit()", ": void");
                        file = updateFile(file, "ngOnInit(): void {",
                            "\r\n\r\n" +
                            "  ngOnDestroy(): void {\r\n" +
                            "    this._unsubscribeAll.next();\r\n" +
                            "    this._unsubscribeAll.complete();\r\n" +
                            "  }", 4);

                        tree.overwrite(filePath, file);
                    }
                });
            return tree;
        },
    ]);
}

function updateFile(file: string, search: string, content: string, extra: number = 0): string {
    let appendIndex = file.indexOf(search);
    file = file.slice(0, appendIndex + search.length + extra) + content + file.slice(appendIndex + search.length + extra);
    return file;
}
